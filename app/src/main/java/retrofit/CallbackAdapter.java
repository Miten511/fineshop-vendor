/*
 * Copyright 2016, The Digicorp Information Systems Pvt. Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package retrofit;

import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;

import retrofit.response.BaseResponse;
import retrofit.response.ErrorResponse;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Callback adapter
 */
public abstract class CallbackAdapter implements Callback<JsonObject> {
    private static Gson gson = new GsonBuilder().setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES).create();

    @Override
    public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
        if (call.isCanceled()) {
            return;
        }
        if (!response.isSuccessful()) {
            ErrorResponse errorResponse;
            try {
                if (response.code() == 401) {
                    JsonObject errJsonObj = gson.fromJson(response.errorBody().string(), JsonObject.class);
                    errorResponse = ErrorResponse.fromJson(errJsonObj.get("error").getAsJsonObject());
                } else {
                    errorResponse = new ErrorResponse(response.errorBody().string());
                }
            } catch (Exception e) {
                errorResponse = ErrorResponse.unknownError();
            }
            onApiResponse(errorResponse, null);
            return;
        }

        JsonObject body = response.body();

        if (body == null) {
            try {
            } catch (Exception e) {
                e.printStackTrace();
            }
            onApiResponse(ErrorResponse.unknownError(), null);
            return;
        }

        try {
            String status = body.get("status").getAsString();
            if (status.equalsIgnoreCase("success")) {
                BaseResponse baseResponse = BaseResponse.fromJson(body);
                onApiResponse(null, baseResponse);
            } else {
                ErrorResponse errorResponse = new ErrorResponse(body.get("message").getAsString());
                onApiResponse(errorResponse, null);
            }
        } catch (Exception e) {
            ErrorResponse errorResponse = new ErrorResponse("Some error occurred. Please try again.");
            onApiResponse(errorResponse, null);
            e.printStackTrace();
        }

    }

    @Override
    public void onFailure(Call<JsonObject> call, Throwable t) {
        ErrorResponse errorResponse = new ErrorResponse("Seems like connectivity issue. Please try again.");
        onApiResponse(errorResponse, null);
    }

    public abstract void onApiResponse(ErrorResponse err, BaseResponse response);
}
