package retrofit;


import java.io.File;

public class ApiUtils {

    public static final String BASE_URL = "";

    public static SOService getSOService() {
        return RetrofitClient.getClient(BASE_URL).create(SOService.class);
    }

    //Media Type
    public static final String MEDIATYPE = "application/json; charset=utf-8";
    //Status
    public static final int STATUS_401 = 401;
    public static final int STATUS_200 = 200;
    public static final int STATUS_400 = 400;

    public static final String NOTIFICATION_TYPE = "notification_type";

}