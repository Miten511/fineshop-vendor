/*
 * Copyright (c) 2016 - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential by BNBJobs
 */

package utils;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.Window;
import android.view.WindowManager;


import com.fineshopvendor.R;

import androidx.core.app.ActivityCompat;
import androidx.core.app.ActivityOptionsCompat;
import androidx.fragment.app.Fragment;

public class ActivityUtils {

    public static void launchActivity(Activity context, Class<? extends Activity> activity,
                                      boolean closeCurrentActivity, Bundle bundle) {
        Intent intent = new Intent(context, activity);

        if (bundle != null) {
            intent.putExtras(bundle);
        }

        context.startActivity(intent);
        if (closeCurrentActivity) {
            context.finish();
        }
    }

    public static void launchActivities(Activity context, Class<? extends Activity> activity,
                                      boolean closeCurrentActivity, Bundle bundle) {
        Intent intent = new Intent(context, activity);

        if (bundle != null) {
            intent.putExtras(bundle);
        }

        context.startActivity(intent);
        if (closeCurrentActivity) {
            context.finish();
        }
        context.overridePendingTransition(R.anim.nothing, R.anim.nothing);
    }

    public static void launchActivity(Context context, Class<? extends Activity> activity,
                                      Bundle bundle) {
        Intent intent = new Intent(context, activity);

        if (bundle != null) {
            intent.putExtras(bundle);
        }

        context.startActivity(intent);

    }

    public static void launchActivityWithOpt(Activity context, Class<? extends Activity> activity,
                                             boolean closeCurrentActivity, Bundle bundle
            , ActivityOptionsCompat options) {
        Intent intent = new Intent(context, activity);

        if (bundle != null) {
            intent.putExtras(bundle);
        }

        context.startActivity(intent, options.toBundle());
        if (closeCurrentActivity) {
            context.finish();
        }
    }


    public static void launchActivity(Activity context, Class<? extends Activity> activity) {
        Intent intent = new Intent(context, activity);
        context.startActivity(intent);
        ActivityCompat.finishAffinity(context);

    }


    public static void launchActivity(Activity context, Class<? extends Activity> activity, boolean closeCurrentActivity) {
        ActivityUtils.launchActivity(context, activity, closeCurrentActivity, null);
    }

    public static void launchStartActivityForResult(Activity context,
                                                    Class<? extends Activity> activity
            , boolean closeCurrentActivity
            , Bundle bundle
            , int requestCode) {
        Intent intent = new Intent(context, activity);

        if (bundle != null) {
            intent.putExtras(bundle);
        }

        context.startActivityForResult(intent, requestCode);
        if (closeCurrentActivity) {
            context.finish();
        }
    }

    public static void launchStartActivityForResult(Fragment fragment, Class<? extends Activity> activity
            , int requestCode, Bundle bdl) {
        Intent intent = new Intent(fragment.getContext(), activity);
        if (bdl != null)
            intent.putExtras(bdl);
        fragment.startActivityForResult(intent, requestCode);
    }

    public static void launchActivityWithClearBackStack(Context context, Class<? extends Activity> activity) {
        Intent intent = new Intent(context, activity);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK |
                Intent.FLAG_ACTIVITY_CLEAR_TASK);
        context.startActivity(intent);
    }

    public static void launchActivityWithClearBackStack(Activity context
            , Class<? extends Activity> activity
            , Bundle bundle) {
        Intent intent = new Intent(context, activity);
        intent.putExtras(bundle);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK |
                Intent.FLAG_ACTIVITY_CLEAR_TASK);
        context.startActivity(intent);
        context.finish();
    }


    /**
     * Force screen to turn on if the phone is asleep.
     *
     * @param context The current Context or Activity that this method is called from
     */
    public static void turnScreenOn(Activity context) {
        try {
            Window window = context.getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD);
            window.addFlags(WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED);
            window.addFlags(WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON);
        } catch (Exception ex) {
            Logger.withTag("Caffeine").log("Unable to turn on screen for activity " + context);
        }
    }

    //Call phone Intent
    public static void callPhone(Context context, String phoneNumber) {
        try {
            Intent dialIntent = new Intent(Intent.ACTION_DIAL);
            dialIntent.setData(Uri.parse("tel:" + phoneNumber));
            context.startActivity(dialIntent);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //Email Intent
    public static void EmailIntent(Context context, String emailid) {
        try {
            Intent intent = new Intent(Intent.ACTION_SENDTO);
            intent.setData(Uri.parse("mailto:"));
            intent.putExtra(Intent.EXTRA_EMAIL, new String[]{emailid + ""});
            intent.putExtra(Intent.EXTRA_SUBJECT, "From Xkeeper");
            context.startActivity(Intent.createChooser(intent, "Email via..."));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


//    public static void openPhotoView(String url, Activity context, Bundle bdl) {
//        bdl.putString(Constants.BUNDLE_PHOTO, url);
////        ActivityOptionsCompat options = ActivityOptionsCompat.
////                makeSceneTransitionAnimation(context, (View) view, url);
//        ActivityUtils.launchActivity(context
//                , ActivityPhotoView.class, false, bdl);
//    }
}