package base;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;

import com.fineshopvendor.R;

import java.util.HashMap;

import androidx.annotation.LayoutRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import retrofit.CallServerApi;
import retrofit2.Response;
import utils.AppUtils;
import utils.Prefs;

/**
 * Created at 23-07-2018
 */
public abstract class BaseFragment<T extends ViewDataBinding> extends Fragment {
    private Dialog progressDialog;
    private Prefs pref;
    private Response response;

    private BaseActivity mActivity;
    private View mRootView;
    private T mViewDataBinding;
    protected CallServerApi callServerApi;

    public void initPref() {
        pref = Prefs.getInstance();

    }


    public void updateResources(Context context, String language) {
        if (getActivity() != null) {
            ((BaseActivity) getActivity()).updateResources(context, language);
        }
    }

    public void showProgressDialog(boolean show) {
        if (show) {
            showProgressDialog();
        } else {
            hideProgressDialog();
        }
    }

    protected void showAlerterBar(Activity activity, String message) {
        AppUtils.showToast(activity, message);
    }


    /**
     * @return layout resource id
     */
    public abstract
    @LayoutRes
    int getLayoutId();


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof BaseActivity) {
            BaseActivity activity = (BaseActivity) context;
            this.mActivity = activity;

        }
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(false);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mViewDataBinding = DataBindingUtil.inflate(inflater, getLayoutId(), container, false);
        mRootView = mViewDataBinding.getRoot();
        callServerApi = new CallServerApi(getContext());
        initPref();
        return mRootView;
    }

    @Override
    public void onDetach() {
        mActivity = null;
        super.onDetach();
    }

    @Override
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mViewDataBinding.setLifecycleOwner(this);
        mViewDataBinding.executePendingBindings();
    }

    public BaseActivity getBaseActivity() {
        return mActivity;
    }

    public T getViewDataBinding() {
        return mViewDataBinding;
    }

    public void hideKeyboard() {
        if (mActivity != null) {
            mActivity.hideKeyboard();
        }
    }

    public boolean isNetworkConnected() {
        return mActivity != null && mActivity.isNetworkConnected();
    }

    protected HashMap<String, String> getHeaderMap() {
        HashMap<String, String> mapHeader = new HashMap<>();
        mapHeader.put("x-auth", "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjVkOGRmYWUxODMyMDljNjM5OGJkNjY3YSIsImlhdCI6MTU2OTU4NTg4OX0.c2S4zzFKWs7qkFmHBD0hWLsfDTORva6CcA43XLfESIM");
        return mapHeader;
    }


    public void showProgressDialog() {
        if (progressDialog == null) {
            progressDialog = new Dialog(getContext());
        } else {
            return;
        }
        View view = LayoutInflater.from(getContext()).inflate(R.layout.app_loading_dialog,
                null, false);

        progressDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        progressDialog.setContentView(view);
        Window window = progressDialog.getWindow();

        window.addFlags(WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED
                | WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD);
        window.addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON
                | WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON);
        if (window != null) {
            window.setBackgroundDrawable(ContextCompat.getDrawable(getContext(), android.R.color.transparent));
        }
        progressDialog.setCancelable(false);
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.show();
    }

    public final void hideProgressDialog() {
        if (progressDialog != null && progressDialog.isShowing() && getActivity() != null && !getActivity().isFinishing()) {
            progressDialog.dismiss();
            progressDialog = null;
        }
    }

    private LinearLayoutManager layoutManager;

    public void setRecyclerViewLaypoutParms(RecyclerView recyclerView) {
        layoutManager = new LinearLayoutManager(getContext()
                , RecyclerView.VERTICAL, false);
        recyclerView.setLayoutManager(layoutManager);
    }

    public LinearLayoutManager getLayoutManager() {
        return layoutManager;
    }

    public void cancelNotification() {
        if (getActivity() != null) {
            ((BaseActivity) getActivity()).cancelNotification();
        }
    }
}
