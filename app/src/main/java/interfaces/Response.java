package interfaces;

import retrofit.response.BaseResponse;

public interface Response<T> {
    void onSuccess(BaseResponse baseResponse);

    void onFail(String message);
}
